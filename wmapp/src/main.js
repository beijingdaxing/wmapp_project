import Vue from 'vue'
import App from './App.vue'
import Vant from 'vant';
import 'vant/lib/index.css';
import axios from 'axios';
import Vuex from 'vuex';
import router from '@/router/router.js'
Vue.use(Vuex);
Vue.prototype.axios=axios;

Vue.use(Vant);
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
