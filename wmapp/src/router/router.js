import Vue from 'vue';
import Router from 'vue-router';
Vue.use(Router);
let router = new Router({
    routes:[
        {name:"home",path:"/home",component:()=>import("@/pages/home/home.vue")},
        {name:"search",path:"/search",component:()=>import("@/pages/search/search.vue")},
        {name:"cart",path:"/cart",component:()=>import("@/pages/cart/cart.vue")},
        {name:"user",path:"/user",component:()=>import("@/pages/user/user.vue")},
        {name:"sort",path:"/sort",component:()=>import("@/pages/sort/sort.vue")},
    ]
});
export default router;