module.exports={
    devServer:{
        open:true,
        proxy:{
            "/api":{
                target:"http://localhost:8848",
                changeOrigin:true,
                pathRewrite:{
                    "^/api":""
                }
            },
            
        }
    }
}